package org.wlld.position;

import org.wlld.tools.LinearRegression;

import java.util.List;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
public class PositionNode {
    private LinearRegression linearRegression;
    private double min;
    private double max;

    public PositionNode(List<Position> positionList) throws Exception {
        min = positionList.get(0).getNorm();
        max = positionList.get(positionList.size() - 1).getNorm();
        linearRegression = new LinearRegression(positionList.size());
        for (Position position : positionList) {
            double[] XY = new double[]{position.getX(), position.getY()};
            linearRegression.insertXY(XY, position.getSq());
        }
        linearRegression.regression();
    }

    public boolean isHere(double x, double y) {
        double norm = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        if (norm >= min && norm <= max) {
            return true;
        } else {
            return false;
        }
    }

    public double getDist(double x, double y) {
        return linearRegression.getValue(x, y);
    }

}
