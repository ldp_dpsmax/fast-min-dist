package org.wlld.position;

import java.util.Comparator;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
public class NormSort implements Comparator<Position> {
    @Override
    public int compare(Position o1, Position o2) {
        if (o1.getNorm() < o2.getNorm()) {
            return -1;
        } else if (o1.getNorm() > o2.getNorm()) {
            return 1;
        }
        return 0;
    }
}
