package org.wlld.position;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
public class Position {
    private double x;
    private double y;
    private double norm;
    private int id;
    private int Uid;//坐标主键
    private double sq;//矩阵序列

    public int getUid() {
        return Uid;
    }

    public void setUid(int uid) {
        Uid = uid;
    }

    public double getSq() {
        return sq;
    }

    public void setSq(double sq) {
        this.sq = sq;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getNorm() {
        return norm;
    }

    public void norm() {
        norm = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }


    public void setY(int y) {
        this.y = y;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
