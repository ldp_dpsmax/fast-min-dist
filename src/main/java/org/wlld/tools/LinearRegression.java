package org.wlld.tools;


import org.wlld.MatrixTools.Matrix;
import org.wlld.MatrixTools.MatrixOperation;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description rgb回归 Y = r *wr + g * wg + b* wb
 */
public class LinearRegression {
    private double w1;
    private double w2;
    private double b;
    private Matrix XY;//坐标矩阵
    private Matrix NormSequence;//内积序列矩阵
    private int xIndex = 0;//记录插入数量
    private boolean isRegression = false;//是否进行了回归

    public LinearRegression(int size) {//初始化rgb矩阵
        XY = new Matrix(size, 3);
        NormSequence = new Matrix(size, 1);
        xIndex = 0;
    }

    public LinearRegression() {
    }

    public void insertXY(double[] xy, double sequence) throws Exception {//rgb插入矩阵
        if (xy.length == 2) {
            XY.setNub(xIndex, 0, xy[0]);
            XY.setNub(xIndex, 1, xy[1]);
            XY.setNub(xIndex, 2, 1.0);
            NormSequence.setNub(xIndex, 0, sequence);
            xIndex++;
        } else {
            throw new Exception("rgb length is not equals three");
        }
    }

    public double getValue(double x, double y) {//获取值
        return w1 * x + w2 * y + b;
    }

    public boolean regression() throws Exception {//开始进行回归
        if (xIndex > 0) {
            Matrix ws = MatrixOperation.getLinearRegression(XY, NormSequence);
            if (ws.getX() == 1 && ws.getY() == 1) {//矩阵奇异
                isRegression = false;
            } else {
                w1 = ws.getNumber(0, 0);
                w2 = ws.getNumber(1, 0);
                b = ws.getNumber(2, 0);
                isRegression = true;
            }
            return isRegression;
            // System.out.println("wr==" + wr + ",wg==" + wg + ",b==" + b);
        } else {
            throw new Exception("regression matrix size is zero");
        }
    }

    public double getW1() {
        return w1;
    }

    public void setW1(double w1) {
        this.w1 = w1;
    }

    public double getW2() {
        return w2;
    }

    public void setW2(double w2) {
        this.w2 = w2;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }
}
