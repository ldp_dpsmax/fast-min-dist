package org.wlld.tools;

import org.wlld.entity.NormPosition;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description 归一化类
 */
public abstract class NormalizationPosition {
    private double minX;
    private double minY;
    private double maxX;
    private double maxY;
    private boolean isXFirst = false;
    private boolean isYFirst = false;

    protected double getMinX() {
        return minX;
    }

    protected double getMinY() {
        return minY;
    }

    protected double getMaxX() {
        return maxX;
    }

    protected double getMaxY() {
        return maxY;
    }

    protected void setMinX(double minX) {
        this.minX = minX;
    }

    protected void setMinY(double minY) {
        this.minY = minY;
    }

    protected void setMaxX(double maxX) {
        this.maxX = maxX;
    }

    protected void setMaxY(double maxY) {
        this.maxY = maxY;
    }

    protected void setXY(double x, double y) {
        if (x < minX) {
            minX = x;
        }
        if (y < minY) {
            minY = y;
        }
        if (x > maxX) {
            maxX = x;
        }
        if (y > maxY) {
            maxY = y;
        }
    }

    public NormPosition norm(double x, double y) {//归一化
        NormPosition normPosition = new NormPosition();
        double xNorm = (x - minX) / (maxX - minX);
        double yNorm = (y - minY) / (maxY - minY);
        normPosition.setNormX(xNorm);
        normPosition.setNormY(yNorm);
        return normPosition;
    }

    protected NormPosition reduction(double x, double y) {//归一化还原
        NormPosition normPosition = new NormPosition();
        double xNorm = x * (maxX - minX) + minX;
        double yNorm = y * (maxY - minY) + minY;
        normPosition.setNormX(xNorm);
        normPosition.setNormY(yNorm);
        return normPosition;
    }
}
